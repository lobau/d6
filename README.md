# d6.rocks

![Screenshot of the app. A 3d dice floating on a wooden background. The traditional faces with dots are replaces with etched rectangles that connect at the edges.](README.png)

A d6 (a six-faced dice) designer.

Deployed at [d6.rocks](https://d6.rocks)


## Run

The project is deployed on render, which offers a drive at the default location `/var/data`
To run it locally, you need to emulate this by creating a `disk/` directory at the root (it's in the .gitignore because it will contain the saved dice).

```
mkdir disk
```

Install dependencies:
```
npm install
```

Run locally (uses `disk/` as stoage and auto-reload on changes)
```
npm run dev
```

To deploy the app (define the disk path in `server.js`)
```
npm start

# Which is just an alias for
node server.js
```