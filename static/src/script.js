import * as THREE from './three.module.js';

import {
    RoundedBoxGeometry
} from './RoundedBoxGeometry.js';

const domain = "d6.rocks";
const _ = query => document.querySelector(query);

const Animator = {
    easeInOut: time => {
        return time < 0.5 ? 4 * time * time * time : (time - 1) * (2 * time - 2) * (2 * time - 2) + 1;
    },
    easeOutBack: time => {
        return Math.pow(time - 1, 2) * ((1.70158 + 1) * (time - 1) + 1.70158) + 1;
    },
    elastic: time => {
        return Math.pow(2, -5 * time) * Math.sin(((time - 0.3 / 4) * (Math.PI * 2)) / 0.3) + 1;
    },
    fanOut: time => {
        return 2 / (1 + Math.pow(1000, -time)) - 1;
    },
    rollercoaster: time => {
        return (-1.15 * Math.sin(time * 7.7)) / (time * 7.7) + 1.15;
    },
    linear: time => {
        return time;
    },
};

const updateFace = (index) => {
    Textures[index].needsUpdate = true;
}

const gotoFace = index => {
    switch (index) {
        case 0: // 1
            rotateTo(0, 270, 0);
            break;
        case 1: // 6
            rotateTo(0, 90, 0);
            break;
        case 2: // 2
            rotateTo(90, 0, 0);
            break;
        case 3: // 5
            rotateTo(270, 0, 0);
            break;
        case 4: // 3
            rotateTo(0, 0, 0);
            break;
        case 5: // 4
            rotateTo(180, 0, 180);
            break;
        default:
            rotateTo(0, 0, 0);
    }
};

const showEditor = (index) => {
    Editors.forEach(editor => {
        // console.log(editor);
        if (editor.index === index) {
            editor.view.style.display = "block";
            editor.toggleButton.className = "selected";
        } else {
            editor.view.style.display = "none";
            editor.toggleButton.className = "";
        }
    });
    gotoFace(index);
}

const Rotation = {
    x: 0,
    y: 0,
    z: 0,
    startX: 0,
    startY: 0,
    startZ: 0,
    goalX: 0,
    goalY: 0,
    goalZ: 0,
    distanceX: 0,
    distanceY: 0,
    distanceZ: 0,
    t: 0,
    p: 0,
    speedX: 0,
    speedY: 0,
    speedZ: 0,
    maxSpeedX: 0,
    maxSpeedY: 0,
    maxSpeedZ: 0,
    isSpinning: true,
};

const rotateTo = (x, y, z) => {
    if (window.spinTimer) {
        clearTimeout(window.spinTimer);
    }

    Rotation.goalX = x;
    Rotation.goalY = y;
    Rotation.goalZ = z;

    Rotation.startX = Rotation.x;
    Rotation.startY = Rotation.y;
    Rotation.startZ = Rotation.z;

    Rotation.distanceX = x - Rotation.x;
    Rotation.distanceY = y - Rotation.y;
    Rotation.distanceZ = z - Rotation.z;

    Rotation.t = 0;
    Rotation.p = 0;

    window.requestAnimationFrame(update);
};

const update = () => {
    Rotation.t += 0.016;
    Rotation.p = Animator.easeOutBack(Rotation.t);
    Rotation.x = (Rotation.startX + Rotation.distanceX * Rotation.p);
    Rotation.y = (Rotation.startY + Rotation.distanceY * Rotation.p);
    Rotation.z = (Rotation.startZ + Rotation.distanceZ * Rotation.p);

    if (Rotation.t < 1) {
        window.requestAnimationFrame(update);
        mesh.rotation.x = Rotation.x * Math.PI / 180;
        mesh.rotation.y = Rotation.y * Math.PI / 180;
        mesh.rotation.z = Rotation.z * Math.PI / 180;
    } else {
        mesh.rotation.x = Rotation.goalX * Math.PI / 180;
        mesh.rotation.y = Rotation.goalY * Math.PI / 180;
        mesh.rotation.z = Rotation.goalZ * Math.PI / 180;
    }
};

const startSpinning = () => {
    Rotation.isSpinning = true;

    Rotation.maxSpeedX = 0.25;
    Rotation.maxSpeedY = 0.35;
    Rotation.maxSpeedZ = -0.45;

    Rotation.accelX = 0.001;
    Rotation.accelY = 0.001;
    Rotation.accelZ = 0.001;

    Rotation.speedX = 0;
    Rotation.speedY = 0;
    Rotation.speedZ = 0;

    if (window.spinTimer) {
        clearTimeout(window.spinTimer);
    }

    window.spinTimer = setTimeout(() => {
        window.requestAnimationFrame(spin);
    }, 3000);
};

const stopSpinning = () => {
    Rotation.isSpinning = false;

    Rotation.accelX = 0;
    Rotation.accelY = 0;
    Rotation.accelZ = 0;

    Rotation.speedX = 0;
    Rotation.speedY = 0;
    Rotation.speedZ = 0;

};

const spin = () => {

    if (Rotation.speedX < Rotation.maxSpeedX) {
        Rotation.speedX += Rotation.accelX;
    }
    if (Rotation.speedY < Rotation.maxSpeedY) {
        Rotation.speedY += Rotation.accelY;
    }
    if (Rotation.speedZ < Rotation.maxSpeedZ) {
        Rotation.speedZ += Rotation.accelZ;
    }

    Rotation.x += Rotation.speedX;
    Rotation.y += Rotation.speedY;
    Rotation.z += Rotation.speedZ;

    Rotation.x = Rotation.x % 360;
    Rotation.y = Rotation.y % 360;
    Rotation.z = Rotation.z % 360;

    mesh.rotation.x = Rotation.x * Math.PI / 180;
    mesh.rotation.y = Rotation.y * Math.PI / 180;
    mesh.rotation.z = Rotation.z * Math.PI / 180;

    if (Rotation.isSpinning) {
        window.requestAnimationFrame(spin);
    }
};

class Editor {
    constructor(param) {
        this.param = param;
        this.parent = this.param.parent || document.body;
        this.index = this.param.index || 0;
        this.size = this.param.size || 360;

        this.view = document.createElement('div');
        this.view.className = 'drawingPad';
        this.view.style.display = "none";
        this.parent.append(this.view);

        this.rawCanvas = document.createElement('canvas');
        this.rawCanvas.style.width = this.size + 'px';
        this.rawCanvas.style.height = this.size + 'px';
        this.rawCanvas.width = this.size;
        this.rawCanvas.height = this.size;
        this.view.append(this.rawCanvas);

        this.fabricCanvas = new fabric.Canvas(this.rawCanvas, {
            backgroundColor: "#fff",
            imageSmoothingEnabled: true,
            enableRetinaScaling: true,
        });
        // this.fabricCanvas.setZoom(1)

        this.backgroundTexture = "/images/steel2_small.jpg";

        this.fabricCanvas.setBackgroundImage(this.backgroundTexture, this.fabricCanvas.renderAll.bind(this.fabricCanvas));

        this.fabricCanvas.on('after:render', () => {
            updateFace(this.index);
        });
        this.fabricCanvas.on('mouse:over', () => {
            stopSpinning();
            gotoFace(this.index);
        });
        this.fabricCanvas.on('mouse:out', () => {
            startSpinning();
        });

        this.fabricCanvas.on('object:moving', (options) => {
            var snapZone = 10;

            // Center X
            var objectMiddleX = options.target.left + options.target.getScaledWidth() / 2;
            if (objectMiddleX > this.fabricCanvas.width / 2 - snapZone &&
                objectMiddleX < this.fabricCanvas.width / 2 + snapZone) {
                options.target.set({
                    left: this.fabricCanvas.width / 2 - options.target.getScaledWidth() / 2,
                }).setCoords();
            }

            // Center Y
            var objectMiddleY = options.target.top + options.target.getScaledHeight() / 2;
            if (objectMiddleY > this.fabricCanvas.height / 2 - snapZone &&
                objectMiddleY < this.fabricCanvas.height / 2 + snapZone) {
                options.target.set({
                    top: this.fabricCanvas.height / 2 - options.target.getScaledHeight() / 2,
                }).setCoords();
            }
        });

        var newID = (new Date()).getTime().toString();

        let placeholderText = new fabric.IText(FaceLabels[this.index], {
            fontFamily: "system-ui, sans-serif, sans",
            fontSize: 100,
            fill: "#333",
            underline: false,
            overline: false,
            myid: newID,
            objecttype: 'text'
        });
        this.fabricCanvas.add(placeholderText);
        this.fabricCanvas.centerObject(placeholderText);

        this.toggleButton = document.createElement('button');
        this.toggleButton.innerHTML = FaceLabels[this.index];
        _("#toggles").append(this.toggleButton);

        this.toggleButton.onclick = () => {
            showEditor(this.index);
        }

        this.texture = new THREE.CanvasTexture(this.rawCanvas);
        this.texture.needsUpdate = true;
        Textures.push(this.texture);

        // this.material = new THREE.MeshPhongMaterial({
        //     map: this.texture,
        //     bumpMap: this.texture,
        //     bumpScale: 0.04,
        //     shininess: 30,
        //     specular: new THREE.Color(1, 1, 1),
        //     envMap: envMap,
        //     reflectivity: 0.8,
        // });
        this.material = new THREE.MeshPhongMaterial({
            map: this.texture,
            bumpMap: this.texture,
            bumpScale: 0.04,
            shininess: 10,
            specular: new THREE.Color(1, 1, 1),
            envMap: textureEquirec,
            reflectivity: 0.5,
        });
        Materials.push(this.material);

        this.overlay = document.createElement('div');
        this.overlay.className = 'overlay';
        this.view.append(this.overlay);

        // this.faceNumber = document.createElement('div');
        // this.faceNumber.className = 'label';
        // this.faceNumber.innerText = parseInt(this.index + 1);
        // this.overlay.append(this.faceNumber);

        this.toolbarView = document.createElement('div');
        this.toolbarView.className = 'fabricToolbox';
        this.view.append(this.toolbarView);

        this.rectButton = document.createElement('button');
        this.rectButton.innerHTML = `🟦`;
        this.toolbarView.append(this.rectButton);

        this.rectButton.addEventListener('click', () => {
            let shape = new fabric.Rect({
                fill: '#333',
                width: 50,
                height: 50
            })
            this.fabricCanvas.add(shape);
            this.fabricCanvas.centerObject(shape);
        });

        this.circleButton = document.createElement('button');
        this.circleButton.innerHTML = `🔵`;
        this.toolbarView.append(this.circleButton);

        this.circleButton.addEventListener('click', () => {
            let shape = new fabric.Circle({
                radius: 28,
                fill: '#333',
            })
            this.fabricCanvas.add(shape);
            this.fabricCanvas.centerObject(shape);
        });

        this.textButton = document.createElement('button');
        this.textButton.innerHTML = `T`;
        this.toolbarView.append(this.textButton);

        this.textButton.addEventListener('click', () => {
            var newID = (new Date()).getTime().toString();

            let shape = new fabric.IText("text", {
                fontFamily: "system-ui, sans-serif, sans",
                fontSize: 100,
                fill: "#333",
                underline: false,
                overline: false,
                myid: newID,
                objecttype: 'text'
            });
            this.fabricCanvas.add(shape);
            this.fabricCanvas.centerObject(shape);
        });

        this.deleteButton = document.createElement('button');
        this.deleteButton.innerHTML = `<svg viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg"><g style="stroke: currentColor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 1.5px;"><path d="M 2 10 L 10 2"></path><path d="M 10.032 9.982 L 2 2"></path></g></svg>`;
        this.toolbarView.append(this.deleteButton);

        this.deleteButton.onclick = () => {
            this.fabricCanvas.remove(this.fabricCanvas.getActiveObject());
        }

        // let undoButton = document.createElement('button');
        // undoButton.innerHTML = `<svg fill="currentColor" viewBox="0 0 24 24"><path d="M1.61 12c.76 0 1.38.57 1.49 1.32a9.01 9.01 0 1 0 17.77-2.89 8.99 8.99 0 0 0-14.7-5.27L7.28 6.3A1 1 0 0 1 6.6 8H2a1 1 0 0 1-1-1V2.41a1 1 0 0 1 1.7-.7l1.34 1.33a11.98 11.98 0 0 1 19.82 7.1A12.02 12.02 0 0 1 12 24 12.02 12.02 0 0 1 .13 13.75 1.51 1.51 0 0 1 1.6 12Z"/></svg>`;
        // toolbar.append(undoButton);
    }

    write() {
        return `${this.parent} ${this.index}`;
    }
}


const FaceLabels = ['1', '6', '2', '5', '3', '4'];
const Editors = [];
const Textures = [];
const Materials = [];

// CAMERA
const camera = new THREE.PerspectiveCamera(50, 1, 1, 1000);
camera.position.set(0, 0, 10);
const scene = new THREE.Scene();

// TEXTURES

var envMap = new THREE.CubeTextureLoader().load([
    '/images/envMuseum/posx.jpg', '/images/envMuseum/negx.jpg',
    '/images/envMuseum/posy.jpg', '/images/envMuseum/negy.jpg',
    '/images/envMuseum/posz.jpg', '/images/envMuseum/negz.jpg'
]);

// const woodBump = loader.load('images/woodMaps/bump.jpg');
const loader = new THREE.TextureLoader();
const woodNorm = loader.load('/images/wood-old-1-NORM.png');
const woodDiff = loader.load('/images/wood-old-1-DIFFUSE.png');
const textureEquirec = loader.load('/images/hdri.jpg');
textureEquirec.mapping = THREE.EquirectangularReflectionMapping;
textureEquirec.colorSpace = THREE.SRGBColorSpace;

const repeatValue = 4; // You can set this to whatever value you like

woodDiff.wrapS = woodDiff.wrapT = THREE.RepeatWrapping;
woodDiff.repeat.set(repeatValue, repeatValue);

woodNorm.wrapS = woodNorm.wrapT = THREE.RepeatWrapping;
woodNorm.repeat.set(repeatValue, repeatValue);

//////////////////////////////////

for (let i = 0; i < FaceLabels.length; i++) {
    let editor = new Editor({
        index: i,
        parent: _("#faces"),
        size: 360
    });
    Editors.push(editor);
}

// LIGHT
// const ambientLight = new THREE.AmbientLight("white", 0.25);
// scene.add(ambientLight);

const spotLightRed = new THREE.DirectionalLight(0xFFFFFF);
spotLightRed.position.set(1, 3, 4);
spotLightRed.target.position.set(0, 0, 1);
spotLightRed.intensity = 0.85;
spotLightRed.castShadow = true;
spotLightRed.shadow.mapSize.width = 1024;
spotLightRed.shadow.mapSize.height = 1024;
scene.add(spotLightRed);
// spotLightRed.shadowMap.dispose();
// spotLightRed.shadowMap = null;

const pointLight1 = new THREE.PointLight("hsl(180, 100%, 60%)");
pointLight1.position.set(-10, 0, 2);
pointLight1.intensity = 1;
// pointLight1.castShadow = true;
pointLight1.shadow.mapSize.width = 1024;
pointLight1.shadow.mapSize.height = 1024;
scene.add(pointLight1);

const pointLight2 = new THREE.PointLight("#ff6600");
pointLight2.position.set(5, 3, 1);
pointLight2.intensity = 1;
// pointLight2.castShadow = true;
pointLight2.shadow.mapSize.width = 1024;
pointLight2.shadow.mapSize.height = 1024;
scene.add(pointLight2);

// GEOMETRY
const renderer = new THREE.WebGLRenderer({
    antialias: true,
    alpha: true,
});
renderer.shadowMap.enabled = true; // Enable shadows

// Geometry
let geometry = new RoundedBoxGeometry(2, 2, 2, 3, 0.08);
// const geometry = new THREE.BoxGeometry(3, 3, 3);

const mesh = new THREE.Mesh(geometry, Materials);
mesh.position.x = -2;
mesh.position.z = 2;
mesh.castShadow = true;
scene.add(mesh);

const floorGeometry = new THREE.PlaneGeometry(35, 35);
const floorMat = new THREE.MeshPhongMaterial({
    color: "white",
    map: woodDiff,
    normal: woodNorm,
    envMap: envMap,
    reflectivity: 0.1,
});
// const floorMat = new THREE.MeshPhongMaterial({
//     color: '#fff',
//     // side: THREE.BackSide,
// });
const floorMesh = new THREE.Mesh(floorGeometry, floorMat);
floorMesh.position.y = -2;
floorMesh.position.z = 0;
floorMesh.rotation.z = Math.PI / 3;
floorMesh.receiveShadow = true;
// floorMesh.castShadow = true;
scene.add(floorMesh);

// renderer.setSize(window.innerWidth, 500);
renderer.setAnimationLoop(animation);
_("#preview3d").appendChild(renderer.domElement);

function animation(time) {
    renderer.render(scene, camera);
}

// Function to handle window resize
function resizePreview() {
    const rect = _("#preview3d").getBoundingClientRect();

    // Update the renderer size to match the container's size
    // renderer.setSize(rect.width, rect.height);

    // let targetWidth = window.innerWidth * 0.6;

    renderer.setSize(window.innerWidth, window.innerHeight);

    // Update the camera aspect ratio to match the new canvas size
    camera.aspect = window.innerWidth / rect.height;
    camera.updateProjectionMatrix();

    // mesh.position.x = -window.innerWidth / 600 - 10;
    mesh.position.x = -1 - window.innerWidth / 1200;
}

// Attach the event listener to window resize
window.addEventListener('resize', resizePreview, false);

// Initial call to set the canvas size properly
showEditor(0);
resizePreview();
startSpinning();

_("#saveButton").onclick = () => {
    const jsonData = {
        diceMat: "metal",
        backMat: "wood",
        1: "",
        6: "",
        2: "",
        5: "",
        3: "",
        4: ""

    };

    Editors.forEach((editor, index) => {
        jsonData[parseInt(index + 1)] = JSON.stringify(editor.fabricCanvas);
    })

    fetch('/save/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(jsonData),
    })
        .then(response => response.text())
        .then(data => {
            let jsonData = JSON.parse(data);
            let route = jsonData.route;
            console.log(route);

            _("#confirmationDialog").open = true;
            _("#confirmationMessage").innerHTML = `
            <h1>Your share link is ready</h1>
            <p>Just share this link to the viewer page.<p>
            <a target="_blank" href="/${route}">https://${domain}/${route}</a></p>`
        })
        .catch(error => console.error('An error occurred:', error));

}