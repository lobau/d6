module.exports = async (route) => {
    const title = "404";
    return `<!DOCTYPE html>
    <html>
    
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>${title}</title>
    </head>
    
    <body>
        <div id="placeholderCard">
            <h1>${title}</h1>
            <p>Page not found</p> 
            <a href="/editor/">Create a new dice</a>
            <a href="#">Source code</a>
            <a href="#">lobau.io</a>
        </div>
    </body>
    
    </html>`;
};
