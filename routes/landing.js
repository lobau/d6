module.exports = async (route, body) => {
    const title = "d6.rocks";
    const repo = "https://gitlab.com/lobau/d6";

    return `<!DOCTYPE html>
    <html>
    
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>${title}</title>
    </head>
    
    <body>
        <div id="placeholderCard">
            <h1>${title}</h1>
            <ul>
                <li><a href="/editor/">Create a new dice</a></li>
                <li><a href="${repo}">Source code</a></li>
                <li><a href="https://lobau.io">Laurent Baumann</a></li>
            </ul>
        </div>
    </body>
    
    </html>`;
};