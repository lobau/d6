module.exports = async (route, data) => {
    const title = "d6.lol";
    return `<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="/src/style.css" />
    
        <script type="text/javascript" src="/src/fabric.min.js"></script>
    </head>
    
    <body>
        <main>
            <section id="preview3d"></section>
            <section id="faces"></section>
            <section id="toggles"></section>
        </main>
    
        <script>const data = ${data};</script>
        <script type="module" src="/src/viewer.js"></script>
    </body>
    
    </html>
    `;
};
