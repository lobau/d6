const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const path = require("path");
const app = express();
app.use(bodyParser.json());

const http = require("http").Server(app);
const port = Number(process.env.PORT) || 8080;
const publicPath = path.resolve(__dirname, 'static');

const route_landing = require("./routes/landing.js");
const route_viewer = require("./routes/viewer.js");
const route_error = require("./routes/error.js");

if (process.env.LOCAL) {
    global.disk = "./disk/";
} else {
    global.disk = "/var/data/";
}

app.use(express.static(publicPath));

app.get('/', (req, res) => {
    route_landing().then(
        function(html) {
            res.writeHead(200, {
                "Content-Type": "text/html"
            });
            res.write(html);
            res.end();
        }.bind(res)
    );
});

app.get('/editor/', (req, res) => {
    res.sendFile(path.join(__dirname, 'static/editor.html'));
});

app.post('/save/', (req, res) => {
    // Retrieve a representation of all the faces of the die
    const jsonObject = req.body;
    const stringified = JSON.stringify(jsonObject);

    // Generate a random document name
    let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let randomID = "";
    for (let i = 0; i < 24; i++) {
        randomID += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    // Write the json content inside the file
    const filePath = global.disk + randomID + ".json";
    fs.writeFile(filePath, stringified, err => {
        if (err) {
            console.error(err);
        }
    });

    let response = {
        status: 200,
        route: randomID
    };

    // Let the frontend know it's done
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(response));
});

app.get('/:route([a-zA-Z0-9]{24})/', (req, res) => {
    const route = req.params.route;

    if (!fs.existsSync(global.disk + route + ".json")) {
        console.log(`No ${route}.json`);

        route_error(route).then(
            html => {
                res.writeHead(200, {
                    "Content-Type": "text/html"
                });
                res.write(html);
                res.end();
            }
        );

    } else {
        fs.readFile(global.disk + route + ".json", {
            encoding: 'utf8',
            flag: 'r'
        }, (error, data) => {
            if (error) {
                console.log(error);
                return;
            }

            route_viewer(route, data).then(
                html => {
                    res.writeHead(200, {
                        "Content-Type": "text/html"
                    });
                    res.write(html);
                    res.end();
                }
            );
        });
    }
});

app.use(function(req, res, next) {
    res.status(404);
    route_error().then(
        html => {
            res.writeHead(200, {
                "Content-Type": "text/html"
            });
            res.write(html);
            res.end();
        }
    );
});

http.listen(port, () => {
    console.log(`listening on http://localhost:${port}`);
});